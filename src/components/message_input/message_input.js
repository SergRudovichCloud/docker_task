import React, { Component } from 'react';
import "./message_input.scss";

export default class MessageInput extends Component {
    constructor(props) {
        super(props);
        this.state = { value: '' };
    }
    handleChange = (event) => {
        this.setState({ value: event.target.value });
    }
    handleSend = () => {
        this.setState({ value: '' });
        this.props.onMessageSend(this.state.value);
    }
    render() {
        return (
            <div className="message-input">
                <textarea
                    className="message-input-text"
                    value={this.state.value}
                    onChange={this.handleChange}
                ></textarea>
                <button
                    className="message-input-button"
                    onClick={this.handleSend}
                >Send</button>
            </div>
        )
    }
}
