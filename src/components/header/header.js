import React, { Component } from 'react';
import moment from 'moment';
import "./header.scss";

export default class Header extends Component {
    render() {
        return (
            <div className="header">
                <div className="header-title">My Chat</div>
                <div>
                    <span className="header-users-count">{this.props.users}</span><span> users</span>
                </div>
                <div>
                    <span className="header-messages-count">{this.props.messages}</span>
                    <span> messages</span>
                </div>
                <div>
                    <span>Last message at </span>
                    <span className="header-last-message-date">{moment(this.props.lastMessageData).format('DD.MM.YYYY HH:mm')}</span>
                    </div>
            </div>
        )
    }
}
