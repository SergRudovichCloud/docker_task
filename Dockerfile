FROM node:alpine

COPY public ./public
COPY src ./src
COPY package.json ./
COPY bsa.js ./
RUN npm install

CMD ["npm", "start"]

EXPOSE 3000